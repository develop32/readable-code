﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NamesForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.JapaneseTextBox = New System.Windows.Forms.TextBox()
        Me.JapaneseButton = New System.Windows.Forms.Button()
        Me.EnglishButton = New System.Windows.Forms.Button()
        Me.EnglishTextBox = New System.Windows.Forms.TextBox()
        Me.NameDataGridView = New System.Windows.Forms.DataGridView()
        CType(Me.NameDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'JapaneseTextBox
        '
        Me.JapaneseTextBox.Location = New System.Drawing.Point(12, 12)
        Me.JapaneseTextBox.Name = "JapaneseTextBox"
        Me.JapaneseTextBox.Size = New System.Drawing.Size(148, 19)
        Me.JapaneseTextBox.TabIndex = 0
        '
        'JapaneseButton
        '
        Me.JapaneseButton.Location = New System.Drawing.Point(166, 10)
        Me.JapaneseButton.Name = "JapaneseButton"
        Me.JapaneseButton.Size = New System.Drawing.Size(75, 23)
        Me.JapaneseButton.TabIndex = 1
        Me.JapaneseButton.Text = "Japanese"
        Me.JapaneseButton.UseVisualStyleBackColor = True
        '
        'EnglishButton
        '
        Me.EnglishButton.Location = New System.Drawing.Point(430, 10)
        Me.EnglishButton.Name = "EnglishButton"
        Me.EnglishButton.Size = New System.Drawing.Size(75, 23)
        Me.EnglishButton.TabIndex = 3
        Me.EnglishButton.Text = "English"
        Me.EnglishButton.UseVisualStyleBackColor = True
        '
        'EnglishTextBox
        '
        Me.EnglishTextBox.Location = New System.Drawing.Point(276, 12)
        Me.EnglishTextBox.Name = "EnglishTextBox"
        Me.EnglishTextBox.Size = New System.Drawing.Size(148, 19)
        Me.EnglishTextBox.TabIndex = 2
        '
        'NameDataGridView
        '
        Me.NameDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NameDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.NameDataGridView.Location = New System.Drawing.Point(12, 55)
        Me.NameDataGridView.Name = "NameDataGridView"
        Me.NameDataGridView.RowTemplate.Height = 21
        Me.NameDataGridView.Size = New System.Drawing.Size(493, 321)
        Me.NameDataGridView.TabIndex = 4
        '
        'NamesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 396)
        Me.Controls.Add(Me.NameDataGridView)
        Me.Controls.Add(Me.EnglishButton)
        Me.Controls.Add(Me.EnglishTextBox)
        Me.Controls.Add(Me.JapaneseButton)
        Me.Controls.Add(Me.JapaneseTextBox)
        Me.Name = "NamesForm"
        Me.Text = "Form1"
        CType(Me.NameDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents JapaneseTextBox As TextBox
    Friend WithEvents JapaneseButton As Button
    Friend WithEvents EnglishButton As Button
    Friend WithEvents EnglishTextBox As TextBox
    Friend WithEvents NameDataGridView As DataGridView
End Class
