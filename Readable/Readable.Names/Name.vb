﻿Friend NotInheritable Class Name

    Public Sub New(japanese As String, english As String)
        Me.Japanese = japanese
        Me.English = english
    End Sub

    Public ReadOnly Property Japanese As String
    Public ReadOnly Property English As String

End Class