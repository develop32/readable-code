﻿Public Class NamesForm
    Private ReadOnly _names As New List(Of Name)

    Public Sub New()

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()
        StartPosition = FormStartPosition.CenterScreen

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        _names.Add(New Name("商品", "Product"))
        _names.Add(New Name("商品名", "ProductName"))
        _names.Add(New Name("在庫", "Stock"))

        NameDataGridView.DataSource = _names

        NameDataGridView.Columns(0).Width = 200
        NameDataGridView.Columns(1).Width = 200
    End Sub

    Private Sub JapaneseButton_Click(sender As Object, e As EventArgs) Handles JapaneseButton.Click
        Dim searchWord As String = JapaneseTextBox.Text.ToLower
        Dim names = _names.Where(Function(n) n.Japanese.ToLower.Contains(searchWord))
        NameDataGridView.DataSource = names.ToList()
    End Sub

    Private Sub EnglishButton_Click(sender As Object, e As EventArgs) Handles EnglishButton.Click
        Dim searchWord As String = EnglishTextBox.Text.ToLower
        Dim names = _names.Where(Function(n) n.English.ToLower.Contains(searchWord))
        NameDataGridView.DataSource = names.ToList()
    End Sub

End Class