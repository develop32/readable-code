﻿''' <summary>
''' クラス
''' アクセス修飾子とNotInheritableを付ける
''' 継承されるクラスはごく少数。とういか少数にするべき。
''' 継承だらけのシステムは読めない。
''' 必要になった場合に「NotInheritable」を外す
''' </summary>
Public NotInheritable Class ReadableCode5Class

End Class

' ------------------------------------------------------
' クラス名はソリューションエクスプローラで並べることを意識する
' ------------------------------------------------------
' GOOD: 大項目→中項目→小項目の順に書いて
' GOOD: カテゴリ毎に並ぶように命名 MeasureRain, MeasureTemperature
' BAD: 語尾にカテゴリを入れる e.g. RainMeasure, TemperatureMeasure
' データベースのテーブル名も同じ

' ------------------------------------------------------
' GOOD: クラス名は名詞か名詞句で命名する
' ------------------------------------------------------
' 名詞 e.g. Cat, Customer
' 名詞句 e.g. GreatCat
' BAD: XxxData, XxxInfo, XxxObjectとかは付けない
' これを言い出すとすべてにつけないといけない
' Customerがカスタマーの情報だし、データだし、オブジェクト

' ------------------------------------------------------
' GOOD: クラス名で継承元や特性を表現する
' ------------------------------------------------------
' パターン名、アルゴリズム名を付ける
' デザインパターンはプログラマ同士がパターン名で会話できるためにも存在している
' ProductFactory
' ProductEntity
' ProductRepository
' 読むのはプログラマなので名前でどういうクラスかを説明するのに役立つ
' GOOD: 継承しているクラス名に語尾を付ける
Public NotInheritable Class IputException
    Inherits Exception

End Class

' インターフェース
' GOOD: 頭にIを付ける
Public Interface IMember

End Interface

' インターフェース
' GOOD: 抽象クラスの語尾にBaseを付ける
Public MustInherit Class MemberBase

End Class

' テストクラス
' GOOD: テストしたいクラス名にTestを付ける