﻿''' <summary>
''' メソッド
''' </summary>
Public Class ReadableCode4
    Private _products As New List(Of Product)

    ' ------------------------------------------------------
    ' メソッド名の付け方
    ' ------------------------------------------------------
    ' 名前は動詞
    Public Sub Save()

    End Sub

    ' BAD: Processは冗長 Process(処理)　それをいったら全部処理
    Public Sub SaveProcess()

    End Sub

    Public Function FindAll()
        Return _products
    End Function

    ' 主キーで検索する場合はByXxxIdはなくても良い
    Public Function Find(productId As Integer) As Product
        Return _products.Find(Function(x) x.ProductId = productId)
    End Function

    ' 同じ型を返す検索が何種類かある場合
    Public Function FindByPrice(price As Integer) As Product
        Return _products.Find(Function(x) x.Price = price)
    End Function

    ' ------------------------------------------------------
    ' SubとFunctionを意識する
    ' ------------------------------------------------------
    ' GOOD: Functionは値を取得のみ
    ' GOOD: Functionは何が取得されるかが分かるように命名
    Public Function GetProduct(productId As Integer) As Product
        Return _products.Find(Function(x) x.ProductId = productId)
    End Function

    ' GOOD: Subは何かを実行する場合のみ
    ' GOOD: Subは何をするのか分かるように命名
    Public Sub InsertProduct(product As Product)
        Console.WriteLine($"追加処理...{product}")
    End Sub

    ' BAD: Functionの中でSub的なことをしない
    Public Function GetProduct3(productId As Integer) As Product
        Dim p = _products.Find(Function(x) x.ProductId = productId)
        ' BAD: Functionの中で何かのアクションをしない
        ' BAD: 使う側からしたらInsertされているとは思わない
        InsertProduct(p)
        Return p
    End Function

    ' GOOD: 骨格と処理に分ける
    Private Sub 使う側()
        ' BAD
        Dim p3 As Product = GetProduct3(1)

        Dim p As Product = GetProduct(1)
        InsertProduct(p)
    End Sub

    ' ------------------------------------------------------
    ' インテリセンスを意識した名前にする
    ' ------------------------------------------------------
    Private Sub クライアントコード70()

    End Sub

    Private Sub Update(product As Product)

    End Sub

    ' BAD
    Private Sub PriceUpdate(product As Product)

    End Sub

    Private Sub UpdatePrice(product As Product)

    End Sub

    Private Sub UpdateStock(product As Product)

    End Sub

    ' ------------------------------------------------------
    ' メソッド名の例
    ' ------------------------------------------------------
    ' GOOD: 生成メソッド: CreateXXXX
    Private Sub クライントコード94()
        Dim p = Product.CreateNullProduct()
    End Sub

    ' GOOD: 異なる型にするとき: ToXXXX
    Private Sub クライントコード100()
        Dim p = Product.CreateNullProduct()
        Dim dto = New ProductDto(p)
        Dim p2 = dto.ToProduct()

    End Sub

    ' ------------------------------------------------------
    ' 無駄に変数を入れて返却しない
    ' ------------------------------------------------------
    Public Function GetProduct110(productId As Integer) As Product
        ' BAD
        Dim result = _products.Find(Function(x) x.ProductId = productId)
        Return result

        ' GOOD
        Return _products.Find(Function(x) x.ProductId = productId)
    End Function

    Public Function GetProduct119(productId As Integer) As Product
        ' GOOD: 変数に入れる意味がある場合はOK
        Dim result = _products.Find(Function(x) x.ProductId = productId)
        result.Stock += 1
        Return result
    End Function

    ' ------------------------------------------------------
    ' 重複をなくす
    ' ------------------------------------------------------
    ' コピーした本人が一番コピーされていることを知っている
    'Public Function GetProduct129(productId As Integer) As Product
    '    ' GOOD: 変数に入れる意味がある場合はOK
    '    Dim result = _products.Find(Function(x) x.ProductId = productId)
    '    result.Stock += 1
    '    Return result
    'End Function

    'Public Function GetProduct137(productId As Integer) As Product
    '    ' GOOD: 変数に入れる意味がある場合はOK
    '    Dim result = _products.Find(Function(x) x.ProductId = productId)
    '    result.Stock += 4
    '    Return result
    'End Function
    Public Function GetProduct143(productId As Integer, addStock As Integer) As Product
        ' GOOD: 変数に入れる意味がある場合はOK
        Dim result = _products.Find(Function(x) x.ProductId = productId)
        result.Stock += addStock
        Return result
    End Function

    ' ------------------------------------------------------
    ' リージョンで区切らない
    ' ------------------------------------------------------

#Region ""

    ' スタイルコップで非推奨
    ' 開くのが面倒
    ' リージョンで囲まないといけないくらい長いクラスは分割できないか検討
    ' みとうしのよいクラスを作る

#End Region

End Class