﻿Imports System.Data.SqlClient
Imports System.IO

''' <summary>
''' 変数
''' </summary>
Friend NotInheritable Class ReadableCode3Field
    ' GOOD: インスタンス変数(メンバ変数)は一番上
    ' 登場人物が把握できる

    ' NOTBAD: privateな変数はアンダーバー「_」を付ける
    Private _sensorValue As Integer = 0

    Private sensorValue As Integer = 0

    Public Sub New(sensorValue As Integer)
        _sensorValue = sensorValue
        Me.sensorValue = sensorValue
    End Sub

    Private Sub SetValue(sensorValue As Integer)
        _sensorValue = sensorValue
        Me.sensorValue = sensorValue
    End Sub

    Private Function GetValue() As Integer
        Dim sensorValue As Integer = 0
        _sensorValue = sensorValue
        Me.sensorValue = sensorValue
        Return Me.sensorValue
    End Function

    ' BAD: ハンガリアン記法を使わない
    Private iPrice As Integer = 0

    Private intPrice As Integer = 0

    ' GOOD
    Private _price As Integer = 0

    Private price As Integer = 0

    ' メソッド内の変数は直前に宣言する
    ' BAD: 最初に全部宣言する
    Private Sub 最初に全部宣言するBAD()
        Dim fileName As String = "c:\xxx\yyy.txt"
        Dim productId As Integer = 0
        Dim productName As String = String.Empty
        Dim price As Integer = 0
        Dim stock As Integer = 0
        Dim products As New List(Of Product)
        Dim dtos As New List(Of ProductDto)

        If Not File.Exists(fileName) Then
            ' ここで抜けた場合、宣言はほとんど無駄。
            Return
        End If

        Dim lines() As String = File.ReadAllLines(fileName)
        If lines.Length < 1 Then
            ' ここで抜けた場合、宣言はほとんど無駄。
            Return
        End If

        For Each line In lines
            ' わざわざ前の値に上書きするのは気持ち悪い
            productId = Convert.ToInt32(line.Substring(0, 2))
            productName = Convert.ToInt32(line.Substring(2, 10))
            price = Convert.ToInt32(line.Substring(12, 2))
            ' productsはここ以外で操作されていないかを確認しないといけない
            products.Add(New Product(productId, productName, price, stock))
        Next

        For Each produt In products
            ' dtos
            dtos.Add(New ProductDto(produt))
        Next

        ' 事前の宣言はノイズになる
        ' 最後の最後でしか使わないインターフェースが宣言されているとノイズ
        ' 長いメソッドの場合、どこで操作しているのかを全部見ないといけない
        ' メソッドの一番上に宣言する変数をすべて並べるのは古い書き方

    End Sub

    ' GOOD: メソッドの内の変数は直前に宣言する
    Private Sub メソッドの内の変数は直前に宣言する()

        Dim fileName As String = "c:\xxx\yyy.txt"
        If Not File.Exists(fileName) Then
            ' ここで抜けた場合、宣言は無駄にならない
            Return
        End If

        Dim lines() As String = File.ReadAllLines(fileName)
        If lines.Length < 1 Then
            ' ここで抜けた場合、宣言は無駄にならない
            Return
        End If

        Dim products As New List(Of Product)
        For Each line In lines
            ' わざわざ前の値に上書きするのは気持ち悪い
            Dim productId As Integer = Convert.ToInt32(line.Substring(0, 2))
            Dim productName As String = Convert.ToInt32(line.Substring(2, 10))
            Dim price As Integer = Convert.ToInt32(line.Substring(12, 2))
            Dim stock As Integer = Convert.ToInt32(line.Substring(14, 3))
            ' productsはここ以外で操作されていないかを確認しないといけない
            products.Add(New Product(productId, productName, price, stock))
        Next

        Dim dtos As New List(Of ProductDto)
        For Each produt In products
            ' dtos
            dtos.Add(New ProductDto(produt))
        Next

        ' 使用する一番近い場所で宣言する
        ' 生存時間を短くする
    End Sub

    ' ループの変数はループ内で宣言する
    Private Sub 無駄な事前の宣言()
        Dim products = ProductSqlServer.GetProducts.ToList()
        ' BAD: 無駄な事前の宣言
        Dim i As Integer = 0
        For i = 0 To products.Count Step 1

        Next
    End Sub

    Private Sub ループの変数はループ内で宣言する()
        Dim products = ProductSqlServer.GetProducts.ToList()
        ' GOOD: ループの変数はループ内で宣言する
        For i = 0 To products.Count Step 1

        Next
    End Sub

    ' 変数を使いまわさない
    Private Sub 変数を使いまわさないBAD(product As Product)
        Dim value As Decimal
        value = product.Price * 0.8
        Console.WriteLine($"シルバー会員{value}")

        ' BAD: 前回の値の影響調査が必要
        ' BAD: 影響がないからいいのではなく、影響がないことを確認する必要があることがBAD
        ' バグの元。影響があった場合はバグる可能性あり
        value = product.Price * 0.6
        Console.WriteLine($"ゴールド会員{value}")
    End Sub

    Private Sub 変数を使いまわさないGOOD(product As Product)
        Dim silverPrice As Decimal = product.Price * 0.8
        Console.WriteLine($"シルバー会員{silverPrice}")

        Dim goldPrice As Decimal = product.Price * 0.6
        Console.WriteLine($"ゴールド会員{goldPrice}")
    End Sub

    ' Boolean
    ' GOOD: Booleanの戻り値はどちらがTrueかを分かるようにする
    ''' <summary>
    ''' 正常かどうかを取得
    ''' </summary>
    ''' <param name="errorCode">エラーコード</param>
    ''' <returns>正常の場合True</returns>
    Private Function IsNormal(errorCode As Integer) As Boolean
        Return errorCode = 0
    End Function

    Private Function IsError(errorCode As Integer) As Boolean
        Return errorCode <> 0
    End Function

    ' BAD: Trueのときにどちらかわからない Trueがエラー？Falseがエラー？
    Public Function CheckError(errorCode As Integer) As Boolean
        Return errorCode = 0
    End Function

    ' 否定形は避ける
    ' BAD
    Private Function NotExists(filePath As String) As Boolean
        Return Not File.Exists(filePath)
    End Function

    ' GOOD
    Private Function Exists(filePath As String) As Boolean
        Return File.Exists(filePath)
    End Function

    ' エラーチェックなどはIsErrorでも問題ない

    ' 開放が必要なオブジェクトにはUsingを使う
    ' BAD
    Private Sub エラー時に開放されない()
        Dim connection As New SqlConnection("XXX")
        Console.WriteLine($"...{connection}")
        ' ↑がエラーになると↓は通らない
        connection.Dispose()
    End Sub

    ' NOTBAD
    Private Sub Finallyで開放する()
        Dim connection As New SqlConnection("XXX")
        Try
            Console.WriteLine($"...{connection}")
        Catch ex As Exception
            Console.WriteLine("error")
        Finally
            connection.Dispose()
        End Try
    End Sub

    ' GOOD
    Private Sub 開放が必要なオブジェクトにはUsingを使う()
        Using connection As New SqlConnection("XXX")
            Console.WriteLine($"...{connection}")
        End Using
    End Sub

End Class