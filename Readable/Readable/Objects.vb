﻿''' <summary>
''' 通常は1ファイル1クラスだが今回は便宜上複数クラス
''' </summary>
Public NotInheritable Class Product

    Public Sub New(productId As Integer, productName As String, price As Integer, stock As Integer)
        Me.ProductId = productId
        Me.ProductName = productName
        Me.Price = price
        Me.Stock = stock
    End Sub

    Public ReadOnly Property ProductId As Integer
    Public ReadOnly Property ProductName As String
    Public ReadOnly Property Price As Integer
    Public Property Stock As Integer

    ' GOOD: 比較演算子はクラスに任せる
    Friend Function CheckStock(stocUseCount As Integer) As Boolean
        Return Stock >= stocUseCount
    End Function

    ' GOOD: 生成メソッド: CreateXXXX
    Friend Shared Function CreateNullProduct() As Product
        Return New Product(1, "", 0, 0)
    End Function

End Class

Public NotInheritable Class ProductDto

    Public Sub New(product As Product)
        Me.ProductId = product.ProductId.ToString
        Me.ProductName = product.ProductName
        Me.Price = product.Price.ToString
        Me.Stock = product.Stock.ToString
    End Sub

    Public ReadOnly Property ProductId As String
    Public ReadOnly Property ProductName As String
    Public ReadOnly Property Price As String
    Public ReadOnly Property Stock As String

    ' GOOD: 異なる型にするとき: ToXXXX
    Public Function ToProduct() As Product
        Return New Product(
            Convert.ToInt32(ProductId),
            ProductName,
            Convert.ToInt32(Price),
            Convert.ToInt32(Stock))
    End Function

End Class

Public Class ProductSqlServer

    ' GOOD: 複数のデータが返却される時は複数形の名前にする
    Public Shared Function GetProducts() As IEnumerable(Of Product)
        Dim result As New List(Of Product)
        result.Add(New Product(10, "p10", 100, 2))
        result.Add(New Product(20, "p20", 200, 6))
        result.Add(New Product(30, "p30", 300, 4))
        Return result
    End Function

    Public Shared Function Exists() As Boolean
        Return True
    End Function

    Public Shared Function NotExists() As Boolean
        Return True
    End Function

    ' GOOD: データが1行の場合は単数形の名前にする
    Friend Shared Function GetProduct(productId As Integer) As Product
        Return New Product(1, "", 1, 1)
    End Function

End Class