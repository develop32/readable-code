﻿Friend Enum Condition
    Setting
    Start
    Running
    [Stop]
    [Error]
End Enum

Public Class ReadableCode
    ' BAD: 良くない書き方
    ' NOTBAD: 別に悪くない
    ' GOOD: 良い書き方, 推奨

    ''' <summary>
    ''' - コード量がは少ない方が良いが、少ない行で書くのが良いコードではない
    ''' - 右に長いより縦に長い方が見易い
    ''' - より少ない時間で読めるか、理解できるかが大事
    ''' - 隣のとなりまで
    ''' </summary>
    Private Sub 右に長いコードを書かない()
        Dim dtos As New List(Of ProductDto)
        ' BAD: 右に長いコードを書かない(隣の隣くらいまでにする)
        ProductSqlServer.GetProducts().Where(Function(x) x.Price > 100).ToList.ForEach(Sub(x) dtos.Add(New ProductDto(x)))

        ' 改行しても右に長いコードには変わらない
        ProductSqlServer.GetProducts().
            Where(Function(x) x.Price > 100).
            ToList.ForEach(Sub(x) dtos.Add(New ProductDto(x)))

        ' ListのForEachするためにToListするのは冗長
        Dim products1 = ProductSqlServer.GetProducts().Where(Function(x) x.Price > 100).ToList
        products1.ForEach(Sub(x) dtos.Add(New ProductDto(x)))

        ' GOOD: 隣のとなりまでしか尋ねない(.が2つくらい)
        Dim products2 = ProductSqlServer.GetProducts().Where(Function(x) x.Price > 100)
        For Each product In products2
            dtos.Add(New ProductDto(product))
        Next

        ' 右に長くなるという事はクラス、メソッドをうまく分割できていない
        ' e.g. GetProductsByPrice(100)のようなメソッドを作る
    End Sub

    ' 判断(If文)
    ' IfとElseがある場合はIfはTrueのときに入る
    Private Sub IfとElseがある場合はIfはTrueのときに入る()
        ' BAD: 肯定形で中に入る
        If Not ProductSqlServer.Exists Then
            ' 否定形(データがないとき)
            Console.WriteLine("処理")
        Else
            ' 肯定形(データがあるとき)
            Console.WriteLine("処理")
        End If

        ' GOOD: 肯定形で中に入る
        If ProductSqlServer.Exists Then
            ' 肯定形(データがあるとき)
            Console.WriteLine("処理")
        Else
            ' 否定形(データがないとき)
            Console.WriteLine("処理")
        End If

        ' GOOD: Elseがない場合は否定形OK
        If Not ProductSqlServer.Exists Then

        End If

        ' BAD: 否定の否定はしない
        If Not ProductSqlServer.NotExists Then

        End If

        ' NOTBAD: 否定の否定はしない
        If ProductSqlServer.NotExists Then

        End If

    End Sub

    Private Sub 比較される値を左(value As Integer)
        ' GOOD: 変化される値を左、固定値を右
        If value < 3 Then

        End If

        ' BAD: 変化される値を右、固定値を左
        If 3 > value Then

        End If

    End Sub

    Private Sub 複数の比較を1回のIf文でやらない(valueA As Integer, valueB As Integer)
        ' ANDの場合

        ' BAD: 複数の比較を1回でやらない
        If valueA = 1 And valueB = 1 Then
            ' 処理
        End If

        ' NOTBAD: 階層を分ける
        If valueA = 1 Then
            If valueB = 1 Then
                ' 処理
            End If
        End If

        ' GOOD: メソッド化を検討する

        ' ORの場合

        ' NOTBAD: 複数の比較を1回でやらない
        If valueA = 1 Or valueB = 1 Then
            ' 処理
        End If

        ' GOOD: メソッド化を検討する
        If IsWriteLogs(valueA, valueB) Then
            ' 処理
        End If

        ' ANDとORが混ざっていたらなおさらメソッド化する
    End Sub

    Private Function IsWriteLogs(valueA As Integer, valueB As Integer) As Boolean
        If valueA = 1 Then
            Return True
        End If
        If valueB = 1 Then
            Return True
        End If
        Return False
    End Function

    Private _isNormal = False

    Private Sub Booleanの比較でTrueやFalseを書かない()
        ' BAD: 冗長なのでTrueは不要
        If _isNormal = True Then

        End If

        ' BAD: 冗長なのでFalseは不要
        If _isNormal = False Then

        End If

        ' GOOD
        If _isNormal Then

        End If

        ' GOOD
        If Not _isNormal Then

        End If

        ' BAD: 無駄に否定形にしない
        If _isNormal <> True Then

        End If

        If _isNormal <> False Then

        End If
    End Sub

    Private Sub 型チェックはAsを使う(obj As Object)
        If TypeOf obj Is Product Then
            ' BAD: ２回型変換が行われて冗長
            Dim product = CType(obj, Product)
            Console.WriteLine("処理{0}", product.ProductName)
        End If

        ' GOOD
        Dim product2 = TryCast(obj, Product)
        If product2 Is Nothing Then
            Console.WriteLine("処理{0}", product2.ProductName)
        End If
    End Sub

    ' メソッドはできるだけ早く抜ける
    '
    Private Function IsStopping1(condition As Condition) As Boolean
        If condition = Condition.Setting Or
            condition = Condition.Stop Or
            condition = Condition.Error Then
            Return True
        End If
        Return False
    End Function

    ' BAD: 返却するBooleanを変数に入れて最後にリターンする
    Private Function IsStopping2(condition As Condition) As Boolean
        Dim result As Boolean = False
        If condition = Condition.Setting Then
            result = True
        ElseIf condition = Condition.Stop Then
            result = True
        ElseIf condition = Condition.Error Then
            result = True
        End If
        ' どんな引数が来ても最後まで読まないといけない
        Return result
    End Function

    ' GOOD
    Private Function IsStopping3(condition As Condition) As Boolean
        Dim result As Boolean = False
        If condition = Condition.Setting Then
            Return True
        End If

        If condition = Condition.Stop Then
            Return True
        End If

        If condition = Condition.Error Then
            Return True
        End If

        Return False
    End Function

    ' BAD
    Private Function チェックを多用する1(product As Product) As Boolean
        If product IsNot Nothing Then
            If product.ProductName IsNot Nothing Then
                If product.ProductName.Length > 3 Then
                    If product.ProductName.Substring(2, 1) = "A" Then
                        Return True
                    End If
                End If
            End If
        End If
        Return False
    End Function

    ' GOOD
    Private Function 対象外のときはすぐに抜ける(product As Product) As Boolean
        ' BAD: チェックが多くてネストが深い
        If product Is Nothing Then
            Return False
        End If

        If product.ProductName Is Nothing Then
            Return False
        End If

        If product.ProductName.Length <= 3 Then
            Return False
        End If
        Return product.ProductName.Substring(2, 1) = "A"
    End Function

    ' GOOD
    Public Sub 都合が悪いケースはガードする(product As Product)
        If product Is Nothing Then
            Throw New ArgumentException("product Is Nothing")
        End If
        Console.WriteLine($"処理・・・{product.ProductName}")
    End Sub

    ' GOOD
    Private Function 必ずやりたい処理はFinallyを使う(product As Product) As Boolean
        Try
            If product Is Nothing Then
                Return False
            End If

            If product.ProductName Is Nothing Then
                Return False
            End If

            If product.ProductName.Length <= 3 Then
                Return False
            End If
            Return product.ProductName.Substring(2, 1) = "A"
        Finally
            Console.WriteLine("必ずやりたい処理")
        End Try

    End Function

    ' GOOD
    Public Sub 比較演算子はできるだけクラスにさせる(
                                 product As Product,
                                 stocUseCount As Integer)
        ' NOTBAD: クライアントコードで比較する
        If product.Stock >= stocUseCount Then
            Console.WriteLine($"処理・・・{product.Stock}")
        End If
        If product.CheckStock(stocUseCount) Then
            Console.WriteLine($"処理・・・{product.Stock}")
        End If
    End Sub

    Private Sub Ifの中括弧の省略はしない(product As Product)
        If product.Price > 100 Then
            Console.WriteLine($"処理・・・{product.Price}")
        End If

        ' BAD: 1行でも。修正しずらい
        If product.Price > 100 Then Console.WriteLine($"処理・・・{product.Price}")
    End Sub

End Class