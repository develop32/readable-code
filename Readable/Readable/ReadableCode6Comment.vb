﻿Imports System.IO

''' <summary>
''' コメント
''' </summary>
Public Class ReadableCode6Comment
    ' ------------------------------------------------------
    ' メソッド内にコメントを書かない
    ' ------------------------------------------------------
    ' GOOD: メソッド内にコメントを書かずXMLコメントにする
    ' コメントを書く理由は？意味のわからないメソッドだから？

    ''' <summary>
    ''' ファイルを読んでプロダクトを生成します
    ''' </summary>
    Private Sub DDD1()

        Dim fileName As String = "c:\xxx\yyy.txt"
        If Not File.Exists(fileName) Then
            Return
        End If

        Dim lines() As String = File.ReadAllLines(fileName)
        If lines.Length < 1 Then
            Return
        End If

        Dim products As New List(Of Product)
        For Each line In lines
            Dim productId As Integer = Convert.ToInt32(line.Substring(0, 2))
            Dim productName As String = Convert.ToInt32(line.Substring(2, 10))
            Dim price As Integer = Convert.ToInt32(line.Substring(12, 2))
            Dim stock As Integer = Convert.ToInt32(line.Substring(14, 3))
            products.Add(New Product(productId, productName, price, stock))
        Next

        Dim dtos As New List(Of ProductDto)
        For Each produt In products
            dtos.Add(New ProductDto(produt))
        Next

    End Sub

    ' ------------------------------------------------------
    ' 分かりづらい部分はメソッド化してメソッド名で想いを伝える
    ' ------------------------------------------------------
    Private Sub DDD2()

        Dim fileName As String = "c:\xxx\yyy.txt"
        If Not File.Exists(fileName) Then
            Return
        End If

        Dim lines() As String = File.ReadAllLines(fileName)
        If lines.Length < 1 Then
            Return
        End If

        Dim products As New List(Of Product)
        For Each line In lines
            Dim p = FileLineToProduct(line)
            'Dim productId As Integer = Convert.ToInt32(line.Substring(0, 2))
            'Dim productName As String = Convert.ToInt32(line.Substring(2, 10))
            'Dim price As Integer = Convert.ToInt32(line.Substring(12, 2))
            'Dim stock As Integer = Convert.ToInt32(line.Substring(14, 3))
            products.Add(p)
        Next

        Dim dtos As New List(Of ProductDto)
        For Each produt In products
            dtos.Add(New ProductDto(produt))
        Next

    End Sub

    ''' <summary>
    ''' ファイルの1行をProductクラスに変換します
    ''' </summary>
    ''' <param name="line">ファイルの1行</param>
    ''' <returns></returns>
    Function FileLineToProduct(line As String) As Product
        Dim productId As Integer = Convert.ToInt32(line.Substring(0, 2))
        Dim productName As String = Convert.ToInt32(line.Substring(2, 10))
        Dim price As Integer = Convert.ToInt32(line.Substring(12, 2))
        Dim stock As Integer = Convert.ToInt32(line.Substring(14, 3))
        Return New Product(productId, productName, price, stock)
    End Function

    ' 主要な処理は各クラスやメソッドに移り、
    ' クライアントコードでやる必要のある骨格だけが残る
    ' これを続けるとほとんどのコードはどこかのクラスに移る

    ' ------------------------------------------------------
    ' コードを読んだ人が「えっ？」と思うことが
    ' 予測される場所にだけコメントを付ける。
    ' ------------------------------------------------------
    Private Sub DDD2()

        Dim fileName As String = "c:\xxx\yyy.txt"
        If Not File.Exists(fileName) Then
            Return
        End If

        Dim lines() As String = File.ReadAllLines(fileName)
        If lines.Length < 1 Then
            Return
        End If

        Dim products As New List(Of Product)
        For Each line In lines
            Dim p = FileLineToProduct(line)
            If p.ProductName(0) = "A" Then
                ' 商品名の頭文字がAの場合は特殊品のため、対応しない
                Continue For
            End If

            products.Add(p)
        Next

        Dim dtos As New List(Of ProductDto)
        For Each produt In products
            dtos.Add(New ProductDto(produt))
        Next

    End Sub

End Class