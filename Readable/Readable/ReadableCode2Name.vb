﻿''' <summary>
''' - 気取らない。分かりやすい名前にする。
''' - 英語略さない(40、50文字くらいはOK)
''' - タイピング量を気にして短くしない→インテリセンスがあるので大丈夫
''' - そんな事よりも、略された文字を後に理解する他人の事を考えるべき。
''' </summary>
Public Class ReadableCode2Name

    ' 意図が明確な名前を付ける
    ' BAD: どんな日付を求めているか分かりづらい
    Private Function DateCheck([date] As Date) As Boolean
        Return True
    End Function

    ' GOOD: 現在日付を求めていることが分かる
    Private Function CurrentDateCheck(currentDate As Date) As Boolean
        Return True
    End Function

    ' 名前は素直に！
    ' 連想ゲーム的な名前は付けない

    ' 【仕様】
    ' 通常はサーバーDBへ書き込み、エラーがでたら再接続するまでローカルDBに書く

    ' GOOD: 素直な名前にする
    Private Enum DatabaseWrite
        Local
        Server
    End Enum

    Private Enum DatabaseWriteMode
        LocalDatabase
        ServerDatabase
    End Enum

    ' BAD: 連想ゲーム的な名前を付けない
    Private Enum RunningMode

        ''' <summary>
        ''' サーバーが切れているからローカルにバッファリングしている
        ''' </summary>
        Buffering

        ServerDatabase
    End Enum

    ' BAD: 否定形の名前を付けない NoServerという事はLocalという連想が必要
    Private _noServerWriteMode = False

    ' BAD: 仕様が分かっている人しか読めない: サーバーがエラーと言うことはLocal書き込み
    ' 知る人ぞ知る名前にしない
    Private _serverErrorMode = False

    ' 基本は仕様をそのまま表現する

    ' NOTBAD: 1つの事しかしていなければ短い名前でも理解できる
    Public Function GetProduct(productId As Integer) As Product
        Dim result As New Product(1, "", 1, 1)
        result.Stock = 2
        Return result
    End Function

    ' 長い名前
    ' 長い名前になるのはうまくクラス分割ができていない可能性がある
    ' メソッド名や変数名が長くなる→専用のクラスにすることを検討する
    Private _normalMeasurementWarningLevelConditionEntity As New List(Of NormalMeasurementWarningLevelConditionEntity)

    Private Function NormalMeasurementWarningLevelConditionEntityExists(id As Integer) As Boolean
        Return _normalMeasurementWarningLevelConditionEntity.Exists(Function(x) x.Id = id)
    End Function

    ' 単数形と複数形で表現する
    Private Sub 単数形と複数形で表現する()
        ' GOOD: データが1行の場合は単数形の名前にする
        Dim product As Product = ProductSqlServer.GetProduct(1)

        ' GOOD: 複数のデータが返却される時は複数形の名前にする
        Dim products As IEnumerable(Of Product) = ProductSqlServer.GetProducts(1)
        ' 変数、メソッド、プロパティ、クラス名、フォルダ名などすべて
        ' productList, productCollectionよりも語尾にsを付ける方が良い
        For Each product2 In products

        Next

        ' 複数形の名前は単純にSを付けるだけとは限らない
        ' Entity→Entities
        ' Factory→Factories
        ' Repository→Repositories
    End Sub

    ' 対になる言葉の組み合わせを決めておく
    ' add / remove
    ' insert / delete
    ' get / set
    ' start / stop
    ' begin / end
    ' send / receive
    ' first / last
    ' put / get
    ' up / down
    ' show / hide
    ' open / close
    ' source / destination
    ' increment / decrement
    ' lock / unlock
    ' old / new
    ' next / previous

    ' 業務で使う名前は統一する
    ' 特にドメイン(業務で使う名前は統一する)
    ' その業界の言葉を英語でどう表現するかは必ず決めておく！
    ' 特に2つ呼び方がある場合は注意
    ' レンタルビデオ(Video, DVD, Content)
    ' 客: Customer, User?
    ' 商品, 製品: Product?
    ' 宴会: Banquet, Party
    ' 体温計: 予測温度, 実温度・・・業界特殊なやつは特に共通意識で実装できるように
    ' 発電所(電力, 水力, 原子力, 新エネルギー)
    ' しっくりくる名前を見つける

    ' とりあえず素直に英語で名付け、長すぎるとか日本に馴染みがない場合
    ' →違うのに変えてみるOK
    ' 略語が一般的な場合は略語でOK
    ' 例えば、CD→Compact Diskのほうが分かりづらい
    ' すべて名前で処理をする。名前は非常に大事

    ' 辞書を作る

    ' 長い名前のクラス
    Friend NotInheritable Class NormalMeasurementWarningLevelConditionEntity
        Friend Property Id As Integer
    End Class

    ' GOOD: 専用クラスを作成する
    Friend NotInheritable Class NormalMeasurementWarningLevelConditionEntities

        ' NOTBAD: 1つの事しかしていなければ短い名前でも理解できる
        Private _entities As New List(Of NormalMeasurementWarningLevelConditionEntity)

        ' NOTBAD: 1つの事しかしていなければ短い名前でも理解できる
        Friend Function Exists(id As Integer) As Boolean
            Return _entities.Exists(Function(x) x.Id = id)
        End Function

    End Class

End Class