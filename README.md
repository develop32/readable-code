# リーダブルコード

- 読みやすいシンプルなコードの書き方
- オブジェクト指向(インターフェース、デザインパターン)は含まない
- コーディングコードは含まない



## コメント

1. コメントで悪いコードを取り繕うことはできない
2. コメントではなくメソッド名で表現する
3. 3行のメソッドを複数作ることでシンプルにならないか？
4. 過去のコードをコメントアウトせず、差分はソース管理で確認